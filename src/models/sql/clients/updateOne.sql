update public.clients
set firstname = COALESCE(${firstname}, firstname),
    surname  = COALESCE(${surname}, surname)
where id = ${clientId}